import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import stats
import pandas as pd

def read_time_from_file(filename, reduction=None):
    file = open(filename, 'r')
    times = []
    for line in file.readlines():
        if reduction is None:
            times.append(float(line))
        else:
            times.append(float(line)/pow(1000, reduction))
    return times


def read_time_from_raw_file(filename):
    file = open(filename, 'r')
    times = []
    for line in file.readlines():
        tokens = line.split()
        for token in tokens:
            try:
                times.append(float(token))
            except:
                pass
    return times


def cumsum(list):
    cumsum_list = [list[0]]
    for index in range(1, len(list)):
        cumsum_list.append(cumsum_list[index-1] + list[index])
    return  cumsum_list

optimized_times = read_time_from_file('optimized.txt')
standard_times = read_time_from_file('standard.txt')

iteration = len(optimized_times)

data_to_plot = [optimized_times, standard_times]

fig = plt.gcf()
# Create an axes instance
ax = fig.add_subplot(111)
# Create the boxplot
bp_dict = ax.boxplot(data_to_plot, vert=False)

for line in bp_dict['medians']:
    # get position data for median line
    x, y = line.get_xydata()[1]  # top of median line
    # overlay median value
    plt.text(x, y + 0.03, '%.3f' % x, horizontalalignment='center')  # draw above, centered

ax.set_title(f'Statistics of {iteration} iteration')
ax.set_xlabel('time (sec)')
ax.xaxis.set_label_coords(1.05, -0.025)
ax.set_yticklabels(['optimized', 'standard'])
# plt.show()

optimized_stats = pd.Series(np.array(optimized_times)).describe()
standard_stats = pd.Series(np.array(standard_times)).describe()

attributes = ['min', '25%', '50%', '75%', 'max', 'mean', 'increment']

print('OPERATION                       min        25%        50%        75%        max        '
      'mean           increment')

files = ['gf_mul', 'gauss_sk', 'gauss_pk', 'compute_linear_map', 'apply_linear_map', 'sk_gen', 'pk_gen']
for file in files:
    optimized_times = read_time_from_raw_file(f'optimized_{file}.txt')
    standard_times = read_time_from_raw_file(f'standard_{file}.txt')
    for type in ['optimized', 'standard']:
        title = type + '_' + file
        optimized_stats = pd.Series(np.array(optimized_times)).describe()
        standard_stats = pd.Series(np.array(standard_times)).describe()
        padding = ' ' * (32 - len(title))
        string = title
        for attribute in attributes:
            if attribute != 'increment':
                if type == 'optimized':
                    element = optimized_stats.get(attribute)
                else:
                    element = standard_stats.get(attribute)
                string = string + padding + '%.6f' % round(element, 6)
                padding = '   '
            elif attribute == 'increment' and type == 'optimized':
                optimized_element = optimized_stats.get('75%')
                standard_element = standard_stats.get('75%')
                diff = standard_element - optimized_element
                increment = (diff / standard_element) * 100
                string = string + padding + '%.6f' % round(increment, 6)
        print(string)
    print('-' * 100)

optimized_times = read_time_from_file('optimized.txt')
standard_times = read_time_from_file('standard.txt')

optimized_stats = pd.Series(np.array(optimized_times)).describe()
standard_stats = pd.Series(np.array(standard_times)).describe()

string = 'optimized key gen'
padding = '               '
for attribute in attributes:
    if attribute != 'increment':
        element = optimized_stats.get(attribute)
        string = string + padding + '%.6f' % round(element, 6)
        padding = '   '
    else:
        optimized_element = optimized_stats.get('75%')
        standard_element = standard_stats.get('75%')
        diff = standard_element - optimized_element
        increment = (diff / standard_element) * 100
        string = string + padding + '%.6f' % round(increment, 6)
print(string)

string = 'standard key gen'
padding = '                '
for attribute in attributes:
    if attribute != 'increment':
        element = standard_stats.get(attribute)
        string = string + padding + '%.6f' % round(element, 6)
        padding = '   '
print(string)
print('-' * 100)


optimized_times = read_time_from_file('GF_mul_opt.txt', 2)
standard_times = read_time_from_file('GF_mul.txt', 2)


plt.cla()   # Clear axis
plt.clf()   # Clear figure
plt.close()

opt_cumsum = cumsum(optimized_times)
standard_cumsum = cumsum(standard_times)

fig = plt.gcf()
ax = fig.add_subplot(111)
ax.set_ylabel('time (sec)')
ax.set_xlabel('iteration')
plt.plot(range(1, 11), standard_cumsum[0:10], 'r', range(1, 11), opt_cumsum[0:10], 'b')
plt.show()
plt.plot(range(1, 129), standard_cumsum, 'r', range(1, 129), opt_cumsum, 'b')
plt.show()

print(opt_cumsum)
print(standard_cumsum)

for index in range(0, 10):
    print(f'({index+1},{round(opt_cumsum[index], 6)})')

for index in range(0, 10):
    print(f'({index+1},{round(standard_cumsum[index], 6)})')
















