var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstwx",
  1: "bgmn",
  2: "bcegmnpst",
  3: "_bcgmn",
  4: "_cdfghilmnprtwx",
  5: "dg",
  6: "_m",
  7: "acemopst",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Modules",
  8: "Pages"
};

