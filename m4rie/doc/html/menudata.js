var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Related Pages",url:"pages.html"},
{text:"Modules",url:"modules.html"},
{text:"Data Structures",url:"annotated.html",children:[
{text:"Data Structures",url:"annotated.html"},
{text:"Data Fields",url:"functions.html",children:[
{text:"All",url:"functions.html"},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Files",url:"files.html",children:[
{text:"File List",url:"files.html"},
{text:"Globals",url:"globals.html",children:[
{text:"All",url:"globals.html",children:[
{text:"_",url:"globals.html#index__"},
{text:"b",url:"globals.html#index_b"},
{text:"c",url:"globals.html#index_c"},
{text:"d",url:"globals.html#index_d"},
{text:"g",url:"globals.html#index_g"},
{text:"i",url:"globals.html#index_i"},
{text:"m",url:"globals.html#index_m"},
{text:"n",url:"globals.html#index_n"}]},
{text:"Functions",url:"globals_func.html",children:[
{text:"_",url:"globals_func.html#index__"},
{text:"b",url:"globals_func.html#index_b"},
{text:"c",url:"globals_func.html#index_c"},
{text:"g",url:"globals_func.html#index_g"},
{text:"m",url:"globals_func.html#index_m"},
{text:"n",url:"globals_func.html#index_n"}]},
{text:"Variables",url:"globals_vars.html"},
{text:"Typedefs",url:"globals_type.html"},
{text:"Macros",url:"globals_defs.html"}]}]},
{text:"Examples",url:"examples.html"}]}
