#include "gf.h"
#include "params.h"
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <inttypes.h>
/*

 g++ -o test_gf_13_128_mul test_gf_13_128_mul.cpp gf.c -mpclmul -fopenmp -O3 -march=native -mtune=native

*/

using namespace std;


int main(void){


    int iterations = 128;

    srand(time(0));
    gf x[128] = {0};
    gf y[128] = {0};
    gf c[128] = {0};
    gf coeff;


    for(long i=0; i<iterations; i++){
        coeff = rand() % 8191;
        x[i] = coeff;
        coeff = rand() % 8191;
        y[i] = coeff;
    }


/*
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    int j = 0;

    fp = fopen("f.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        x[j] = atoi(line);
        y[j] = atoi(line);
        j++;
    }

    fclose(fp);
    if (line)
        free(line);
*/

    double start = omp_get_wtime();
    for(int i = 0; i < iterations; i++){
        GF_mul(c, x, y);
    }
    double end = omp_get_wtime();    

    auto begin = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < iterations; i++){ 
        GF_mul(c, x, y);
    }
    auto finish = std::chrono::high_resolution_clock::now();           
    double duration = chrono::duration_cast<chrono::nanoseconds>(finish-begin).count();

    printf("GF_mul over %d iterations\n", iterations);
    printf("total time %lf sec\n", end - start);
    cout << "total time " << duration << " ns\n";
    cout << "mean " << (duration/(iterations)) << " ns\n";

    for(int i = 0; i < iterations; i++){ 
        begin = std::chrono::high_resolution_clock::now();
        GF_mul(c, x, y);
        finish = std::chrono::high_resolution_clock::now();           
        duration = chrono::duration_cast<chrono::nanoseconds>(finish-begin).count();
        cout << duration << " ns\n";
    }

    return 0;
}