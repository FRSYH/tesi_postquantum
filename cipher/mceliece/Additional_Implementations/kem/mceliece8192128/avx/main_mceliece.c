#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nist/rng.h"
#include "crypto_kem.h"
#include <time.h>
#include "operations.h"
#include "pk_gen.h"

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_CRYPTO_FAILURE  -4
#define VERBOSE
//#define TEST

void fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L);


unsigned char entropy_input[48];
unsigned char seed[48];


void
fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L)
{
	unsigned long long i;

	fprintf(fp, "%s", S);

	for ( i=0; i<L; i++ )
		fprintf(fp, "%02X - %lli\n", A[i], i);

	if ( L == 0 )
		fprintf(fp, "00");

	fprintf(fp, "\n");
}


/*
compilare direttamente il seguente file con
gcc -O3 -march=native -mtune=native -Wall -I. -o main_mceliece main_mceliece.c nist/rng.c aes256ctr.c benes.c bm.c controlbits.c decrypt.c encrypt.c fft.c fft_tr.c gf.c int32_minmax_x86.c int32_sort.c operations.c pk_gen.c sk_gen.c uint32_sort.c vec256.c  consts.S syndrome_asm.S transpose_64x128_sp_asm.S transpose_64x256_sp_asm.S update_asm.S vec128_mul_asm.S vec256_ama_asm.S vec256_maa_asm.S vec256_mul_asm.S vec_reduce_asm.S -lkeccak -lcrypto -ldl -L libkeccak.a -lkeccak -lcrypto -ldl -L ../../../../../../generic64 -I ../../../../../../generic64/libkeccak.a.headers -no-pie -fopenmp


per creare libkeccak seguire la procedura del link
https://gist.github.com/mimoo/746205bc29e171ba8ad5b75793283057
*/

int main(int argc, char const *argv[]){

    int ret_val;
    int i;
    unsigned char *ct = 0;
    unsigned char *ss = 0;
    unsigned char *ss1 = 0;
    unsigned char *pk = 0;
    unsigned char *sk = 0;
    
    clock_t start, end;

/*
    unsigned char entropy_input[] = {
        0xaa, 0xe7, 0xd7, 0x4e, 0x3c, 0x3a, 0x52, 0xdd,
        0x87, 0xc7, 0x2a, 0xa4, 0x38, 0x54, 0x7e, 0x37,
        0x1e, 0x97, 0x29, 0x78, 0x22, 0xa2, 0xcd, 0x83,
        0x43, 0x64, 0x84, 0xcf, 0x77, 0x6b, 0x9e, 0xa5,
        0x53, 0xf3, 0x50, 0xc5, 0xc7, 0x8d, 0x46, 0xb3,
        0xa5, 0xf2, 0xe3, 0x99, 0x63, 0x10, 0x1d, 0x10
    };
    unsigned char nonce[48];
*/

#ifdef TEST

    start = clock();
    //INITIALIZATION
    for (i=0; i<48; i++)
        entropy_input[i] = i;
    randombytes_init(entropy_input, NULL, 256);

    randombytes(seed, 48);

    randombytes_init(seed, NULL, 256);

    ct = malloc(crypto_kem_CIPHERTEXTBYTES);
    ss = malloc(crypto_kem_BYTES);
    ss1 = malloc(crypto_kem_BYTES);
    pk = malloc(crypto_kem_PUBLICKEYBYTES);
    sk = malloc(crypto_kem_SECRETKEYBYTES);

    //KEYGEN
    if ( (ret_val = crypto_kem_keypair(pk, sk)) != 0) {
        printf("crypto_kem_keypair returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    //ENCAPSULATION
    if ( (ret_val = crypto_kem_enc(ct, ss, pk)) != 0) {
        printf("crypto_kem_enc returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    //DECAPSULATION
    if ( (ret_val = crypto_kem_dec(ss1, ct, sk)) != 0) {
        printf("crypto_kem_dec returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    end = clock();

    //TEST
    if ( memcmp(ss, ss1, crypto_kem_BYTES) ) {
        printf("Test: FAIL\n");
        return KAT_CRYPTO_FAILURE;
    }else{
        printf("Test: PASS\n");
    }

    printf("%lf\n", ((double) (end-start)/CLOCKS_PER_SEC));

#else

    double time = 0.0, total_time = 0.0, total_time_mean = 0.0, total_sk_time = 0.0, total_pk_time = 0.0;
    int iterations = 100;

    if(argc > 1){
        iterations = atoi(argv[1]);
    }

/*
    FILE *fp = NULL;
    int it = 0;
*/
    
    for(int j=0; j<iterations; j++){
    
        for (i=0; i<48; i++)
            entropy_input[i] = i;
        randombytes_init(entropy_input, NULL, 256);

        randombytes(seed, 48);

        randombytes_init(seed, NULL, 256);
    
    /*
        if ((fp = fopen("/dev/urandom", "r"))) {
            if ((sizeof(entropy_input) != fread(entropy_input, 1, sizeof(entropy_input), fp)) ||
                (sizeof(nonce) != fread(nonce, 1, sizeof(nonce), fp))) {
                break;
            }
        }
        fclose(fp);
        memcpy(&entropy_input[48-sizeof(it)], &it, sizeof(it));
        randombytes_init(entropy_input, nonce, 256);
    */


        ct = malloc(crypto_kem_CIPHERTEXTBYTES);
        ss = malloc(crypto_kem_BYTES);
        ss1 = malloc(crypto_kem_BYTES);
        pk = malloc(crypto_kem_PUBLICKEYBYTES);
        sk = malloc(crypto_kem_SECRETKEYBYTES);

        start = clock();
        if ( (ret_val = crypto_kem_keypair(pk, sk)) != 0) {
            printf("crypto_kem_keypair returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;

        //for(int i = 0 ; i < crypto_kem_SECRETKEYBYTES ; ++i) printf("%x", sk[i]);

        printf(" keypair took %lfs\n\n", time);

        //fprintBstr(stdout, "pk = ", pk, crypto_kem_PUBLICKEYBYTES);


        start = clock();
        if ( (ret_val = crypto_kem_enc(ct, ss, pk)) != 0) {
            printf("crypto_kem_enc returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;
        printf(" encaps took %lfs\n\n", time);

        start = clock();
        if ( (ret_val = crypto_kem_dec(ss1, ct, sk)) != 0) {
            printf("crypto_kem_dec returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;
        printf(" decaps took %lfs\n\n", time);


        if ( memcmp(ss, ss1, crypto_kem_BYTES) ) {
            printf("Test: FAIL\n");
            return KAT_CRYPTO_FAILURE;
        }else{
            printf("Test: PASS\n");
        }


        for(int i = 0 ; i < crypto_kem_BYTES ; ++i) printf("%x", ss1[i]);
        printf("\n\n----------------------------------------------\n");
        total_sk_time += sk_time;
        total_pk_time += pk_time;
        total_time_mean += total_time;
        total_time = 0.0;
        sk_time = 0.0;
        pk_time = 0.0;
        
        /*
        free(ss1);
        free(ss);
        free(ct);
        free(sk);
        free(pk);
        */
    }

    printf("\n---------------STATISTICS-------------------\n");      
    printf("\nmean sk %lf\n", total_sk_time/iterations);
    printf("\nmean pk %lf\n", total_pk_time/iterations);
    printf("\nmean cipher time %lfs\n", total_time_mean/iterations);        
    printf("\n\n----------------------------------------------\n");

#endif

    return 0;
}
