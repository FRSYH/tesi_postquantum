#include "gf.h"
#include "params.h"
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <ctime>
/*

g++ -o test_gf_13_mul test_gf_13_mul.cpp -fopenmp -O3 -march=native -mtune=native

*/

using namespace std;

gf my_gf_mul(gf in0, gf in1)
{
	int i;

	uint64_t tmp;
	uint64_t t0;
	uint64_t t1;
	uint64_t t;

	t0 = in0;
	t1 = in1;

	tmp = t0 * (t1 & 1);

	for (i = 1; i < GFBITS; i++)
		tmp ^= (t0 * (t1 & (1 << i)));

	//

	t = tmp & 0x1FF0000;
	tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	t = tmp & 0x000E000;
	tmp ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	return tmp & GFMASK;
}

int main(void){



    gf a = 1500;
    gf b = 3565;
    gf res;
    int iterations = 10000000;

    double start = omp_get_wtime();
    for(int i = 0; i < iterations; i++){
        res = gf_mul(a, b);
    }
    double end = omp_get_wtime();    

    auto begin = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < iterations; i++){ 
        res = gf_mul(a, b);
    }
    auto finish = std::chrono::high_resolution_clock::now();           
    double duration = chrono::duration_cast<chrono::nanoseconds>(finish-begin).count();

    printf("gf_mul over %d iterations\n", iterations);
    printf("total time %lf sec\n", end - start);
    cout << "total time " << duration << " ns\n";
    cout << "mean " << (duration/(iterations)) << " ns\n";

    return 0;
}