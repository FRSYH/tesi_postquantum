# Contenuto Repo 
* **cipher**: contiene il sorgente dei cifrari presi in considerazione nella fase di ottimizzazione. Il contenuto è immacolato, non è stato modificato in alcun modod.
* **generic64** e **m4rie**: contengono delle librerie necessarie per il progetto
* **ottimizzazioni**: luogo di test per ottimizzazioni ed idee riguardo il progetto. 
* **optimized_chiper**: contiene l'implementazione ottimizzata dei cifrari sotto esame. In questo caso McEliece
* **test**: utilities e dati temporanei per test di performance e simili

# Esecuzione 

L'implementazione di McEliece presa in considerazione nella fase di ottimizzazione è AVX:
`tesi_postquantum/optimized_cipher/mceliece/Additional_Implementations/kem/mceliece8192128/avx`

L'entrypoint di questa implementazione è `main_mceliece.c`. 


### Compilazione entry point ###
`gcc -O3 -march=native -mtune=native -mfpmath=sse -Wall -I. -o main_mceliece main_mceliece.c nist/rng.c aes256ctr.c benes.c bm.c controlbits.c decrypt.c encrypt.c fft.c fft_tr.c gf.c int32_minmax_x86.c int32_sort.c operations.c pk_gen.c sk_gen.c uint32_sort.c vec256.c  consts.S syndrome_asm.S transpose_64x128_sp_asm.S transpose_64x256_sp_asm.S update_asm.S vec128_mul_asm.S vec256_ama_asm.S vec256_maa_asm.S vec256_mul_asm.S vec_reduce_asm.S -lkeccak -lcrypto -ldl -L libkeccak.a -lkeccak -lcrypto -ldl -L ../../../../../../generic64 -I ../../../../../../generic64/libkeccak.a.headers -no-pie -fopenmp  -Wno-unused-variable`



# Confronto caratteristiche cifrari code base

| Chiper           | Mult time %   | Field type   |    grado polinomi in GF2x   |  grado polinomi in GF2EX |
| ---------------- |:-------------:| ------------ |:---------------------------:|:------------------------:|
| LEDA-5-2         | 4             | GF2x         |                             |                          |
| BIKE-3           | 5             | GF2X         | 45000                       |                          |
| RQC-256          | 13.832        | GF2EX        | 137                         | 131                      |
| ROLLO-III-256    | 16.906        | GF2EX + GF2X | 131                         | 67                       |
| HQC-256-3        | 42.844        | GF2X         | 70000                       |                          |
| NTS-13-136       | 71.273        | GF2X         | 13                          |                          |


Mult time % = percentuale del tempo trascorso ad eseguire moltiplicazione rispetto al tempo totale di esecuzione del cifario (keygen + encaps + decaps).


# Note sui cifrari #

## BIKE ##
Si utilizzano codici quasi ciclici che vengono rappresentati da matrici quadrate circolari. Le matrici r x r possono essere viste come dei polinomi, ogni riga viene mappata su un polinomio.
Il campo utilizzato è $`R = F_2 [X]/(X^r - 1)`$. In tutte e tre le varianti si effettuano moltiplicazioni tra polinomi di dimensione attorno a 5000 bytes, i vettori moltiplicati raggiungono il grado 45000. Non si parla di campo esteso $`2^m`$ ma solo di righe di matrici circolari interpretate come polinomi. Il tempo passato a fare moltiplicazione è un piccola percentuale in tutte le varianti con tutte le combinazioni di parametri. Viste le implementazioni e le ottimizzazioni già presenti, difficilmente si potranno individuare ulteriori ottimizzazioni.

## HQC ##
Anche HQC fa uso di codici ciclici, e anche in questo caso le righe delle matici circolari vengono interpretate come polinomi in $`R = F_2 [X]/(X^n -1)`$. Nella configurazione massima NIST cat 5 i polinomi raggiungono il grado 70000. Queste dimensioni spiegano l'alta percentuale di tempo trascorsa ad eseguire la moltiplicazione. I prodotti sartoriali non posso essere utilizzati in questo caso data la dimensione ????

## LEDA ##

## NTS ##
In NTS si effettua una trasformazione della matrice di parity check $`Hm`$ ($`F_{2^m}^{t   \times n}`$) nella matrice $`H`$ ($`F_2^{mt \times n}`$) tramite l'operatore $`B(\cdot)^T`$. La moltiplicazione tra polinomi viene utilizzata proprio in questo opertore. La funzione ff_mul_13 esegui il prodotto tra polinomi nel campo $`F_{13}`$ e riduce il polinomio risultante modulo il polinomio = $`x^{13} + x^4 + x^3 + x + 1`$. NTS rappresenta i polinomi in modo particolare, invece di utiizzare la normale notazione come vettore di elementi $`F_2`$, utilizza la codifica decimale del valore di tale vettore. Esempio [0,1,1,0,1] vettore di 5 elementi in GF2 --> NTS rappresenta tale vettore con il valore decimale della stringa binaria rappresentata, in questo caso 13. I prodotti vengono quindi calcolati in modo apposito utilizzando maschere e shift per determinare il valore corretto. Come metodo di moltiplicazione sembra essere molto efficiente, APPROFONDIRE.

## ROLLO ##
Operazioni su elementi in $`F_{2^m}^n`$, ovvero vettori di n elementi dove gli elementi sono vettori in $`F_{2^m}`$. Per ROLLO-III-256 n = 67, m = 131. In questo caso è possibile utilizzare il prodotto sartoriale per effettuare il prodotto in GF2X mantenendo invece la moltiplicazione NTL su GF2EX.

## RQC ##
Praticamente identico a ROLLO nella sua implementazione a differenza dei codici utilizzati. Stesso operazioni su elementi in $`F_{2^m}^n`$. Per RQC-III n = 131, m = 137. stessa osservazione per i prodotti sartoriali di ROLLO.

# Ottimizzazioni #

## NTS ##
* Rimozione funzione ff_mul_13 da nts_kem.c overhead troppo elevato (rimossi quasi 0.011 sec)
* Rimozione funzione ff_reduce_13 da nts_kem.c overhead troppo elevato (rimossi quasi 0.133 sec)
* 

# Tecnica di moltiplicazione utilizzata (NTL + gf2x) #
| Chiper           | GF2EX         | GF2X                         |
| ---------------- |:-------------:| ---------------------------- |
| LEDA             |  custom       | custom                       |
| BIKE-3           |               | toom + mul basecase (6-7, 3) |
| RQC              | Kron          | toom + mul basecase (5, 9)   |
| ROLLO-III        | Kron          | toom + mul basecase (7-9)    |
| HQC              |               | toom + mul basecase (7-9)    |
| NTS              |               | mul basecase 1               |
| Classic McEliece | Kron          | toom + mul basecase (8-9)    |

* mul basecase = formule di karatsuba esplicite, dimensione da 1 a 9 (numero di variabili a 64 bit --> mul_basecase 3 == moltiplicazione di due polinomi rappresentati da vettori di 3 elementi unsigned long).
* Kron = sostituzione di Kronecker, smonta i polinomi GF2EX in polinomi GF2X ed esegue la moltiplicazione di tali polinomi nel campo esteso inferiore (GF2X).
* toom = moltiplicazione di toom-cook


# Ottimizzazioni GAUSS #
* parallelismo su riduzione sottomatrice: OpenMP vs Pthread



# Installazione #
## gmp ##
```
wget https://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.lz
sudo apt-get install lzip
tar --lzip -xvf gmp-6.1.2.tar.lz
cd gmp-6.1.2
./configure
make
make install
```
## gf2x ##
```
wget https://gforge.inria.fr/frs/download.php/file/38243/gf2x-1.3.0.tar.gz
gunzip gf2x-1.3.0.tar.gz
tar xf gf2x-1.3.0.tar
cd gf2x-1.3.0.tar
./configure
make
make install
```
## NTL ##
```
wget https://www.shoup.net/ntl/ntl-11.4.1.tar.gz
gunzip ntl-11.4.1.tar.gz
tar xf ntl-11.4.1.tar
cd ntl-11.4.1.tar
./configure NTL_GF2X_LIB=on
make
make install
```
## installare keccak ##
```
git clone https://github.com/gvanas/KeccakCodePackage.git
cd KeccakCodePackage
make generic64/libkeccak.a
```
