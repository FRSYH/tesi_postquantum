#include <inttypes.h>

// ---------------------------------------------------------------------
// Small helper template functions (only API)
// In the below x[limbs] only means that x consists of limbs number of
// 64-bit blocks and the operation to be done on the whole x as a vector
// or a big value
// ---------------------------------------------------------------------

// set x[limbs] to zero
template<int limbs>inline void gf2_set_zero(uint64_t * x){
	
	for (int i = 0; i < limbs; i++)	{
		x[i] = 0UL;
	}	
}

// copy destination[limbs] = source[limbs]
template<int limbs>inline void gf2_copy(uint64_t * destination, uint64_t * source){
	for (int i = 0; i < limbs; i++)	{
		destination[i] = source[i];
	}		
}

// computes to[limbs] = to[limbs] XOR from[limbs]
template<int limbs>inline void gf2_xoradd(uint64_t * to, uint64_t * from){
	for (int i = 0; i < limbs; i++)	{
		to[i] = to[i] ^ from[i];
	}	
}

// shifts the long value x[limbs] to the RIGHT by 0<shift<64 bits
template<int limbs, int shift>inline void gf2_shr1const(uint64_t * x){
	for (int i = 0; i < limbs; i++)	{
		x[i] >> shift;
	}
}

// shifts the long value x[limbs] to the LEFT by 0<shift<64 bits
template<int limbs, int shift>inline void gf2_shl1const(uint64_t * x){
	for (int i = 0; i < limbs; i++)	{
		x[i] << shift;
	}
}


#define ATST(i) (algorithm & (1UL<<(i))) /* test bit i of algorithm */

template<uint32_t algorithm, uint64_t data> inline void reduction_templ(unsigned long * r){

	const int n1 = (data>>40)&0xff; // size uint64_t[n1] for the 1st round
	const int n2 = (data>>48)&0xff; // size uint64_t[n2] for the 2nd round
	const int doff = (data>> 0)&0xff; // is d>>3, byte offset
	const int dmsk = (data>>56)&0xff; // is d&7, bit mask
	const int off0 = (data>> 8)&0xff; // offset for 1st reduction bit
	const int off1 = (data>>16)&0xff; // offset for 2nd reduction bit
	const int off2 = (data>>24)&0xff; // offset for 3rd reduction bit
	const int off3 = (data>>32)&0xff; // offset for 4th reduction bit
	const int sh0 = (algorithm>> 0)&7;// shift for 1st reduction bit
	const int sh1 = (algorithm>> 7)&7;// shift for 2nd reduction bit
	const int sh2 = (algorithm>>14)&7;// shift for 3rd reduction bit
	const int sh3 = (algorithm>>21)&7;// shift for 4th reduction bit

	// ==== ROUND 1 ====
	uint64_t w[n1+1];
	gf2_copy<n1>(w, (uint64_t*)(((uint8_t*)r) + doff)); // copy upper bits

	if(ATST(28)) // care about byte overflow
		w[n1] = ((uint8_t*)r)[(n1<<4)-1];
	else
		w[n1] = 0;

	// zeroizing upper bits that has been transferred to w[]
	if(ATST(29)) // if d is not aligned (d 6= 0~mod8)
	{ 
		gf2_set_zero<n1>((uint64_t*)(((uint8_t*)r) + doff + 1));
		((uint8_t*)r)[doff] &= dmsk;
	}
	else
		gf2_set_zero<n1>((uint64_t*)(((uint8_t*)r) + doff));

	// should I apply the mask after loading?
	if(ATST(30))
		w[0] &= ~(uint64_t)dmsk;

	#define AROUND1(k)\
		if(ATST(k*7+3))\
		{ 	if(ATST(k*7+5)) gf2_shr1const<n1+1, sh##k>(w);\
				else gf2_shr1const<n1, sh##k>(w);\
			gf2_xoradd<n1>(r, w);\
		}\
		else /* conditional shift left 0..63 */\
		{ 
			if(ATST(k*7+4))\
				if(ATST(k*7+5)) gf2_shl1const<n1+1, sh##k>(w);\
				else gf2_shl1const<n1, sh##k>(w);\
			gf2_xoradd<n1>((uint64_t*)(((uint8_t*)r) + off##k), w);\
			if(ATST(k*7+6)) /* take care of byte overflow, if any */\
				((uint8_t*)r)[off##k + (n1<<3)] ^= ((uint8_t*)w)[n1<<3];\
		}

	AROUND1(0);
	AROUND1(1);
	if(!ATST(31)) /* stop if hw=3, continue if hw=5 */
	{ 
		AROUND1(2);
		AROUND1(3);
	}

	// ==== ROUND 2 ====
	gf2_copy<n2>(w, (uint64_t*)(((uint8_t*)r) + doff));

	// zeroizing upper X
	if(ATST(29)) // if d is not aligned
	{ 
		gf2_set_zero<n2>((uint64_t*)(((uint8_t*)r) + doff + 1));
		((uint8_t*)r)[doff] &= dmsk;
	}
	else
		gf2_set_zero<n2>((uint64_t*)(((uint8_t*)r) + doff));
	
	// should I apply the mask after loading?
	if(ATST(30))
		w[0] &= ~(uint64_t)dmsk;

	#define AROUND2(k)\
		if(ATST(k*7+3)) /* shift right 1..63 */\
		{ 
			gf2_shr1const<n2, sh##k>(w);\
			gf2_xoradd<n2>(r, w);\
		}\
		else /* conditional shift left 0..63 */\
		{ 
			if(ATST(k*7+4))\
				gf2_shl1const<n2, sh##k>(w);\
			gf2_xoradd<n2>((uint64_t*)(((uint8_t*)r) + off##k ), w);\
		}

	AROUND2(0);
	AROUND2(1);
	if(!ATST(31)) /* stop if hw=3, continue if hw=5 */
	{ 
		AROUND2(2);
		AROUND2(3);
	}
}

#undef ATST
#undef AROUND1
#undef AROUND2