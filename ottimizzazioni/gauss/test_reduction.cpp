#define _GNU_SOURCE
#include <unistd.h>
#include <m4rie/m4rie.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <inttypes.h>
#include <time.h>
#include "gf.h"
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <m4rie/gf2e.h>
#include <m4rie/gf2x.h>
#include <ctime>  
#include <cstdlib>
#include <iostream>
#include <chrono>
#include <ctime>
/*
g++ -O3 -march=native -mtune=native -mfpmath=sse -no-pie -I/include -mmmx -msse -msse2 -msse3 -I/include -mmmx -msse -msse2 -msse3 -I.. -g -I/include -mmmx -msse -msse2 -msse3 -o test_reduction  test_reduction.cpp gf.c -L/lib /usr/local/lib/libm4rie.so -lm4ri -lm -fopenmp -pthread
*/

using namespace std;

int main(void){
   
    gf2e *ff;
    int field = 13;
    ff = gf2e_init(irreducible_polynomials[field][1]);
 
    printf("TEST GF2X arith\n\n");
    word a, b, res;
    deg_t deg = 13;

    a = 1500;
    b = 3565;

    printf("a %" PRIu64 ", b %" PRIu64 "\n", a, b);
    
	auto begin = std::chrono::high_resolution_clock::now();           
    res = gf2x_mul(a, b, deg);
    auto finish = std::chrono::high_resolution_clock::now();           
	unsigned long duration = std::chrono::duration_cast<std::chrono::nanoseconds>(finish-begin).count();  
    printf("gf2x mul %" PRIu64 " - took %lu ns\n", res, duration);

    res = ff->mul(ff, a, b);
    printf("gf2e mul %" PRIu64 "\n", res);

    return 0;
}