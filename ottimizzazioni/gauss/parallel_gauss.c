#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>
#include <omp.h>

#define SYS_T 1664

// gcc -o parallel_gauss parallel_gauss.c -fopenmp -O3
// GOMP_CPU_AFFINITY="0 1 2 3"

int main(void){

    int j, tid;
    int iteration = 1;
    double start, end;
    
    omp_set_num_threads(2);
    for(int i=0; i<iteration; i++){
        start = omp_get_wtime();
        #pragma omp parallel private(j, tid)
        {
            for(j = 0; j < SYS_T; j++){
                
                //tid = omp_get_thread_num();
                #pragma omp single
                {
                    tid++;
                }
                
                tid++;
                #pragma omp barrier
            }
        }
        end = omp_get_wtime();
        printf("parallel computation took: %lfs\n", end-start);
    }

    printf("Max threads %d\n", omp_get_max_threads());

    

    return 0;
}