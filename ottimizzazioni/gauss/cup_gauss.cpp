#include <stdio.h>
#include <stdlib.h>
#include <fflas-ffpack/ffpack/ffpack.h>
#include <fflas-ffpack/field/field-traits.h>
#include "givaro/givpoly1.h"
#include <givaro/gfq.h>
#include <givaro/extension.h>
#include <givaro/modular-integral.h>
#include <givaro/givprint.h>

using namespace Givaro;


template<class Field>
void TestField(const Field& F) {
    std::cerr << "Within " ;
    F.write(std::cerr );
    std::cerr  << " : " << std::flush;

    typename Field::Element a, b, c, d;
    F.init(a, 8191U);
    F.init(b, 8191UL);

    F.init(c);            // empty constructor
    F.init(d);            // empty constructor

    F.add(c, a, b);       // c = a+b

    // Separate output writing
    F.write( std::cout, a) << " + " << std::flush;
    F.write( std::cout, b) << " = " << std::flush;
    F.write( std::cerr, c) << std::endl;

    std::cerr << "Within " ;
    F.write(std::cerr );
    std::cerr  << " : " << std::flush;

    F.mul(c, a, b);     // c = a*b
    F.write( std::cout, a) << " * " << std::flush;
    F.write( std::cout, b) << " = " << std::flush;
    F.write( std::cerr, c) << std::endl;

    F.axpy(d, a, b, c); // d = a*b + c;

    // Writing all outputs in a single command line
    F.write( std::cerr << "Within " ) << " : " << std::flush;
    F.write( F.write( F.write( F.write(
                                       std::cout, c) << " + ", a) << " * ", b) << " = ", d) << std::endl;

    {
        typename Field::Element e;
        F.init(e); F.assign(e,d);
        F.maxpy(e, a, b, d); // e = d-a*b

        // Writing all outputs in a single command line
        F.write( std::cerr << "Within " ) << " : " << std::flush;
        F.write( F.write( F.write( F.write(
                                           std::cout, d) << " - ", a) << " * ", b) << " = ", e) << std::endl;
    }

    {
        typename Field::Element e;
        F.init(e); F.assign(e,d);
        F.maxpyin(e, a, b); // e = d - a*b;

        // Writing all outputs in a single command line
        F.write( std::cerr << "Within " ) << " : " << std::flush;
        F.write( F.write( F.write( F.write(
                                           std::cout, d) << " - ", a) << " * ", b) << " = ", e) << std::endl;
    }

    {
        typename Field::Element e;
        F.init(e); F.assign(e,d);
        F.axmy(e, a, b, d); // e = a*b -d;

        // Writing all outputs in a single command line
        F.write( std::cerr << "Within " ) << " : " << std::flush;
        F.write( F.write( F.write( F.write(
                                           std::cout, a) << " * ", b) << " - ", d) << " = ", e) << std::endl;
    }

    {
        typename Field::Element e;
        F.init(e); F.assign(e,d);
        F.maxpyin(e, a, b); // e = d - a*b;

        // Writing all outputs in a single command line
        F.write( std::cerr << "Within " ) << " : " << std::flush;
        F.write( F.write( F.write( F.write(
                                           std::cout, d) << " - ", a) << " * ", b) << " = ", e) << std::endl;
    }



    // Four operations
    F.write( F.write( std::cout, a) << " += ",  b) << " is " ;
    F.write( std::cout, F.addin(a, b) ) << "   ;   ";
    F.write( F.write( std::cout, a) << " -= ",  b) << " is ";
    F.write( std::cout, F.subin(a, b) ) << "   ;   ";
    F.write( F.write( std::cout, a) << " *= ",  b) << " is " ;
    F.write( std::cout, F.mulin(a, b) ) << "   ;   ";
    F.write( F.write( std::cout, a) << " /= ",  b) << " is ";
    F.write( std::cout, F.divin(a, b) ) << std::endl;


    F.init(a,22996);
    F.inv(b,a);
    F.write( F.write( std::cout << "1/", a) << " is ", b) << std::endl;
    F.mul(c,b,a);
    F.write( std::cout << "1 is ", c) << std::endl;
    F.init(a,22996);
    F.init(b,22996);
    F.write( std::cout << "1/", a) << " is ";
    F.invin(a);
    F.write( std::cout, a) << std::endl;
    F.mulin(a,b);
    F.write( std::cout << "1 is ", a) << std::endl;

    F.init(a,37403);
    F.inv(b,a);
    F.write( F.write( std::cout << "1/", a) << " is ", b) << std::endl;
    F.mul(c,b,a);
    F.write( std::cout << "1 is ", c) << std::endl;
    F.init(a,37403);
    F.init(b,37403);
    F.write( std::cout << "1/", a) << " is ";
    F.invin(a);
    F.write( std::cout, a) << std::endl;
    F.mulin(a,b);
    F.write( std::cout << "1 is ", a) << std::endl;

}

extern "C" {
# include <sys/time.h>
# include <sys/resource.h>
}



int main(void){
    
    // g++ -o cup_gauss cup_gauss.cpp -lgivaro -lgmp -lgmpxx  -fopenmp -lopenblas

//-------------------------------------------------
    //Givaro finite field

    /*
    polinomio irriducibile: x^13 + x^4 + x^3 + x + 1
    codifica binaria del polinomio irriducibile che forma il campo: 10000000011011
    */

    std::vector< GFqDom<int32_t>::Residu_t > Irred(14);
    Irred[0] = 1; Irred[1] = 1; Irred[2] = 0; Irred[3] = 1;
    Irred[4] = 1; Irred[5] = 0; Irred[6] = 0; Irred[7] = 0;
    Irred[8] = 0; Irred[9] = 0; Irred[10] = 0; Irred[11] = 0;
    Irred[12] = 0; Irred[13] = 1;
    GFqDom<int32_t> F8192(2,13, Irred);
    F8192.write(std::cout << "This is the field with 8192 elements: ") << ", using: " << F8192.irreducible() << " as irreducible polynomial" << std::endl;
    TestField( F8192 );

//-------------------------------------------------

    //FFPACK ludivine decomposition

    size_t m,n;
    m = 129UL;
    n = 128UL;

    // Reading the matrix from a file
    int32_t *A;

        Givaro::Modular<int32_t> F(13);

    size_t * P = FFLAS::fflas_new<size_t>(m);
    size_t * Q = FFLAS::fflas_new<size_t>(n);

    size_t r = FFPACK::ReducedColumnEchelonForm(F8192, m, n, A, n, P, Q, false, FFPACK::FfpackSlabRecursive);



}