/*
  This file is for functions for field arithmetic
*/

#include "gf.h"

#include "params.h"
#include <gf2x/gf2x-small.h>
#include <gf2x.h>


gf gf_iszero(gf a)
{
	uint32_t t = a;

	t -= 1;
	t >>= 19;

	return (gf) t;
}

inline gf gf_add(gf in0, gf in1)
{
	return in0 ^ in1;
}

static inline uint64_t direct_mul1(unsigned long a, unsigned long b){
    __m128i aa = _gf2x_mm_setr_epi64(a, 0);
    __m128i bb = _gf2x_mm_setr_epi64(b, 0);
	return _mm_cvtsi128_si64x(_mm_clmulepi64_si128(aa, bb, 0));
}

inline gf gf_mul(gf in0, gf in1){
	
	uint64_t t, prod;
	prod = direct_mul1(in0, in1);

	//riduzione in modulo
	t = prod & 0x1FF0000;
	prod ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	t = prod & 0x000E000;
	prod ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	return prod & GFMASK;
}

/* input: field element in */
/* return: (in^2)^2 */
static inline gf gf_sq2(gf in)
{
	const uint64_t M[] = {0x0001FF0000000000, 
	                      0x000000FF80000000, 
	                      0x000000007FC00000, 
	                      0x00000000003FE000};

	unsigned long prod[2] = {0};
    unsigned long prod2[2] = {0};
	gf2x_mul1(prod, (unsigned long) in, (unsigned long) in);    
    gf2x_mul1(prod2, prod[0], prod[0]);    

	uint64_t x = prod2[0]; 
	uint64_t t;

	for (int i = 0; i < 4; i++)
	{
		t = x & M[i];
		x ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
	}

	return x & GFMASK;
}

/* input: field element in, m */
/* return: (in^2)*m */
static inline gf gf_sqmul(gf in, gf m)
{
	const uint64_t M[] = {0x0001FF0000000000, 
	                      0x000000FF80000000, 
	                      0x000000007FC00000, 
	                      0x00000000003FE000};

	unsigned long prod[2] = {0};
    unsigned long prod2[2] = {0};
	gf2x_mul1(prod, (unsigned long) in, (unsigned long) in);    
    gf2x_mul1(prod2, prod[0], (unsigned long) m);    

	uint64_t x = prod2[0]; 
	uint64_t t;

	for (int i = 0; i < 4; i++)
	{
		t = x & M[i];
		x ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
	}

	return x & GFMASK;
}

/* input: field element in, m */
/* return: ((in^2)^2)*m */
static inline gf gf_sq2mul(gf in, gf m){
    gf tmp = gf_sq2(in);
    return gf_mul(tmp, m);
}

/* input: field element den, num */
/* return: (num/den) */
gf gf_frac(gf den, gf num)
{
	gf tmp_11;
	gf tmp_1111;
	gf out;

	tmp_11 = gf_sqmul(den, den); // ^11
	tmp_1111 = gf_sq2mul(tmp_11, tmp_11); // ^1111
	out = gf_sq2(tmp_1111); 
	out = gf_sq2mul(out, tmp_1111); // ^11111111
	out = gf_sq2(out);
	out = gf_sq2mul(out, tmp_1111); // ^111111111111

	return gf_sqmul(out, num); // ^1111111111110 = ^-1
}

gf gf_inv(gf den)
{
	return gf_frac(den, ((gf) 1));
}

/* input: in0, in1 in GF((2^m)^t)*/
/* output: out = in0*in1 */
void GF_mul(gf *out, gf *in0, gf *in1)
{
	int i, j;

	gf prod[ SYS_T*2-1 ];

	for (i = 0; i < SYS_T*2-1; i++)
		prod[i] = 0;

	for (i = 0; i < SYS_T; i++)
		for (j = 0; j < SYS_T; j++)
			prod[i+j] ^= gf_mul(in0[i], in1[j]);

	//
 
	for (i = (SYS_T-1)*2; i >= SYS_T; i--)
	{
		prod[i - SYS_T + 5] ^= gf_mul(prod[i], (gf) 7682);
		prod[i - SYS_T + 3] ^= gf_mul(prod[i], (gf) 2159);
		prod[i - SYS_T + 0] ^= gf_mul(prod[i], (gf) 3597);
	}

	for (i = 0; i < SYS_T; i++)
		out[i] = prod[i];
}

static inline gf my_rem(unsigned long a){

	unsigned long t;
	t = a & 0x1FF0000;
	a ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);
	t = a & 0x000E000;
	a ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	return a & GFMASK;	
}