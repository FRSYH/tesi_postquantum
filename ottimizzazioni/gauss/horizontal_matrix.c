#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <omp.h>
#include <inttypes.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>

#define GFBITS 13
#define SYS_T 128

int main(void){

    // gcc -o horizontal_matrix horizontal_matrix.c -O3

	int flag = 0;
	int H = 1664;
	int WW = 128;
	int W_end = 127;
	int W = 26;
    int i, j;

    uint64_t mat[ GFBITS * SYS_T ][ 128 ];
	uint64_t mat_t[128][1664];
	
    srand(time(0));


	for(i=0; i < H; i++){
		for(j = 0; j < WW; j++){
            mat[i][j] = rand();
            //printf("%" PRIu64 " ", mat_t[i][j]);
		}
        //printf("\n");
	}
    

	for(i = 0; i < H; i++){
		for(j = 0; j < WW; j++){
			mat_t[W_end-j][i] = mat[i][j];
		}
	}

	for(i = 0; i < H; i++){
		for(j = 0; j < WW; j++){
            if(mat_t[W_end-j][i] != mat[i][j]){
                flag = 1;
                break;
            }
		}
	}


	if(flag == 0){
		printf("HORIZONTAL MATRIX PASS\n");
	}else{
		printf("HORIZONTAL MATRIX FAIL\n");
	}

/*
    for(i = 0; i < WW; i++){
		for(j = 0; j < H; j++){
            printf("%" PRIu64 " ", mat_t[i][j]);
		}
        printf("\n");
	}
*/


    return 0;
}