Test 0

# num_Gz_trials 1
# KeyGen : (1) random_goppa_polynomial consumes 1511736 cycles
# KeyGen : (2) random_permutation consumes 2878629 cycles
# KeyGen : (3) create_matrix_G consumes 287695403 cycles
 keypair took 0.128631s

# Encap : (1-6) encapsulation consumes 484144 cycles
 encaps took 0.000712s

# Decap : (1c) compute_syndrome consumes 1671966 cycles
# Decap : (1d) berlekamp_massey consumes 463877 cycles
# Decap : (1e) roots_finding consumes 98766 cycles
# Decap : (1f) get_e_prime consumes 1563 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 46102 cycles
# Decap : (4) kr_and_status consumes 23697 cycles
 decaps took 0.000840s


Total time 0.130183s


----------------------------------------------
Test 1

# num_Gz_trials 2
# KeyGen : (1) random_goppa_polynomial consumes 1206048 cycles
# KeyGen : (2) random_permutation consumes 2288815 cycles
# KeyGen : (3) create_matrix_G consumes 197765667 cycles
 keypair took 0.095195s

# Encap : (1-6) encapsulation consumes 732192 cycles
 encaps took 0.000278s

# Decap : (1c) compute_syndrome consumes 1895422 cycles
# Decap : (1d) berlekamp_massey consumes 346877 cycles
# Decap : (1e) roots_finding consumes 61129 cycles
# Decap : (1f) get_e_prime consumes 1303 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 46019 cycles
# Decap : (4) kr_and_status consumes 23055 cycles
 decaps took 0.000863s


Total time 0.096336s


----------------------------------------------
Test 2

# num_Gz_trials 1
# KeyGen : (1) random_goppa_polynomial consumes 1045443 cycles
# KeyGen : (2) random_permutation consumes 2213907 cycles
# KeyGen : (3) create_matrix_G consumes 199643010 cycles
 keypair took 0.095649s

# Encap : (1-6) encapsulation consumes 578418 cycles
 encaps took 0.000223s

# Decap : (1c) compute_syndrome consumes 1716275 cycles
# Decap : (1d) berlekamp_massey consumes 484226 cycles
# Decap : (1e) roots_finding consumes 103622 cycles
# Decap : (1f) get_e_prime consumes 1279 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 105982 cycles
# Decap : (4) kr_and_status consumes 43419 cycles
 decaps took 0.000894s


Total time 0.096766s


----------------------------------------------
Test 3

# num_Gz_trials 2
# KeyGen : (1) random_goppa_polynomial consumes 1196186 cycles
# KeyGen : (2) random_permutation consumes 2233728 cycles
# KeyGen : (3) create_matrix_G consumes 198870482 cycles
 keypair took 0.095322s

# Encap : (1-6) encapsulation consumes 699438 cycles
 encaps took 0.000266s

# Decap : (1c) compute_syndrome consumes 1904383 cycles
# Decap : (1d) berlekamp_massey consumes 345287 cycles
# Decap : (1e) roots_finding consumes 95000 cycles
# Decap : (1f) get_e_prime consumes 1612 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 45981 cycles
# Decap : (4) kr_and_status consumes 23614 cycles
 decaps took 0.000879s


Total time 0.096467s


----------------------------------------------
Test 4

# num_Gz_trials 3
# KeyGen : (1) random_goppa_polynomial consumes 1324204 cycles
# KeyGen : (2) random_permutation consumes 2230845 cycles
# KeyGen : (3) create_matrix_G consumes 198876107 cycles
 keypair took 0.095661s

# Encap : (1-6) encapsulation consumes 555536 cycles
 encaps took 0.000214s

# Decap : (1c) compute_syndrome consumes 1794782 cycles
# Decap : (1d) berlekamp_massey consumes 384304 cycles
# Decap : (1e) roots_finding consumes 61696 cycles
# Decap : (1f) get_e_prime consumes 1482 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 46073 cycles
# Decap : (4) kr_and_status consumes 23663 cycles
 decaps took 0.000842s


Total time 0.096717s


----------------------------------------------
Test 5

# num_Gz_trials 2
# KeyGen : (1) random_goppa_polynomial consumes 1207944 cycles
# KeyGen : (2) random_permutation consumes 2221126 cycles
# KeyGen : (3) create_matrix_G consumes 197634200 cycles
 keypair took 0.095521s

# Encap : (1-6) encapsulation consumes 562841 cycles
 encaps took 0.000218s

# Decap : (1c) compute_syndrome consumes 1973689 cycles
# Decap : (1d) berlekamp_massey consumes 378927 cycles
# Decap : (1e) roots_finding consumes 65653 cycles
# Decap : (1f) get_e_prime consumes 1263 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 47264 cycles
# Decap : (4) kr_and_status consumes 24237 cycles
 decaps took 0.000946s


Total time 0.096685s


----------------------------------------------
Test 6

# num_Gz_trials 1
# KeyGen : (1) random_goppa_polynomial consumes 1330465 cycles
# KeyGen : (2) random_permutation consumes 2494904 cycles
# KeyGen : (3) create_matrix_G consumes 212456209 cycles
 keypair took 0.100496s

# Encap : (1-6) encapsulation consumes 601358 cycles
 encaps took 0.000231s

# Decap : (1c) compute_syndrome consumes 2084815 cycles
# Decap : (1d) berlekamp_massey consumes 340082 cycles
# Decap : (1e) roots_finding consumes 61076 cycles
# Decap : (1f) get_e_prime consumes 1301 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 45995 cycles
# Decap : (4) kr_and_status consumes 23592 cycles
 decaps took 0.000929s


Total time 0.101656s


----------------------------------------------
Test 7

# num_Gz_trials 3
# KeyGen : (1) random_goppa_polynomial consumes 1243860 cycles
# KeyGen : (2) random_permutation consumes 2213287 cycles
# KeyGen : (3) create_matrix_G consumes 199552566 cycles
 keypair took 0.095638s

# Encap : (1-6) encapsulation consumes 601426 cycles
 encaps took 0.000231s

# Decap : (1c) compute_syndrome consumes 1860216 cycles
# Decap : (1d) berlekamp_massey consumes 344620 cycles
# Decap : (1e) roots_finding consumes 62879 cycles
# Decap : (1f) get_e_prime consumes 1242 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 46079 cycles
# Decap : (4) kr_and_status consumes 23755 cycles
 decaps took 0.000852s


Total time 0.096721s


----------------------------------------------
Test 8

# num_Gz_trials 3
# KeyGen : (1) random_goppa_polynomial consumes 1331613 cycles
# KeyGen : (2) random_permutation consumes 2171885 cycles
# KeyGen : (3) create_matrix_G consumes 197237040 cycles
 keypair took 0.095830s

# Encap : (1-6) encapsulation consumes 625958 cycles
 encaps took 0.000241s

# Decap : (1c) compute_syndrome consumes 1893776 cycles
# Decap : (1d) berlekamp_massey consumes 339673 cycles
# Decap : (1e) roots_finding consumes 60755 cycles
# Decap : (1f) get_e_prime consumes 1271 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 45844 cycles
# Decap : (4) kr_and_status consumes 22894 cycles
 decaps took 0.000859s


Total time 0.096930s


----------------------------------------------
Test 9

# num_Gz_trials 2
# KeyGen : (1) random_goppa_polynomial consumes 1243367 cycles
# KeyGen : (2) random_permutation consumes 2371316 cycles
# KeyGen : (3) create_matrix_G consumes 194325438 cycles
 keypair took 0.094228s

# Encap : (1-6) encapsulation consumes 551883 cycles
 encaps took 0.000213s

# Decap : (1c) compute_syndrome consumes 1781783 cycles
# Decap : (1d) berlekamp_massey consumes 342334 cycles
# Decap : (1e) roots_finding consumes 61810 cycles
# Decap : (1f) get_e_prime consumes 1241 cycles
# Decap : (2-3) permute_e_and_recover_ke consumes 46231 cycles
# Decap : (4) kr_and_status consumes 23489 cycles
 decaps took 0.000825s


Total time 0.095266s


----------------------------------------------


0.100373
NTS-KEM(13, 136) test: PASS
