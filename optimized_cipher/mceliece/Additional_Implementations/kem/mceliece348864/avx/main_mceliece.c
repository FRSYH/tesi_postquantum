#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nist/rng.h"
#include "crypto_kem.h"
#include <time.h>
#include "operations.h"
#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_CRYPTO_FAILURE  -4
#define VERBOSE
//#define TEST

void fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L);

unsigned char entropy_input[48];
unsigned char seed[48];

/*
compilare direttamente il seguente file con
gcc -O3 -march=native -mtune=native -Wall -I. -o main_mceliece main_mceliece.c nist/rng.c aes256ctr.c benes.c bm.c controlbits.c decrypt.c encrypt.c fft.c fft_tr.c gf.c int32_minmax_x86.c int32_sort.c operations.c pk_gen.c sk_gen.c uint32_sort.c vec256.c  consts.S syndrome_asm.S transpose_64x256_sp_asm.S transpose_64x64_asm.S update_asm.S vec128_mul_asm.S vec256_mul_asm.S vec_mul_asm.S vec_mul_sp_asm.S vec_reduce_asm.S    -lkeccak -lcrypto -ldl -L /home/martino/Documents/generic64 -I /home/martino/Documents/generic64/libkeccak.a.headers -no-pie


per creare libkeccak seguire la procedura del link
https://gist.github.com/mimoo/746205bc29e171ba8ad5b75793283057
*/

int main(int argc, char const *argv[]){

    int ret_val;
    int i;
    unsigned char *ct = 0;
    unsigned char *ss = 0;
    unsigned char *ss1 = 0;
    unsigned char *pk = 0;
    unsigned char *sk = 0;
    
    clock_t start, end;


#ifdef TEST

    start = clock();
    //INITIALIZATION
    for (i=0; i<48; i++)
        entropy_input[i] = i;
    randombytes_init(entropy_input, NULL, 256);

    randombytes(seed, 48);

    randombytes_init(seed, NULL, 256);

    ct = malloc(crypto_kem_CIPHERTEXTBYTES);
    ss = malloc(crypto_kem_BYTES);
    ss1 = malloc(crypto_kem_BYTES);
    pk = malloc(crypto_kem_PUBLICKEYBYTES);
    sk = malloc(crypto_kem_SECRETKEYBYTES);

    //KEYGEN
    if ( (ret_val = crypto_kem_keypair(pk, sk)) != 0) {
        printf("crypto_kem_keypair returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    //ENCAPSULATION
    if ( (ret_val = crypto_kem_enc(ct, ss, pk)) != 0) {
        printf("crypto_kem_enc returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    //DECAPSULATION
    if ( (ret_val = crypto_kem_dec(ss1, ct, sk)) != 0) {
        printf("crypto_kem_dec returned <%d>\n", ret_val);
        return KAT_CRYPTO_FAILURE;
    }

    end = clock();

    //TEST
    if ( memcmp(ss, ss1, crypto_kem_BYTES) ) {
        printf("Test: FAIL\n");
        return KAT_CRYPTO_FAILURE;
    }else{
        printf("Test: PASS\n");
    }

    printf("%lf\n", ((double) (end-start)/CLOCKS_PER_SEC));

#else

    double time = 0.0, total_time = 0.0, total_time_mean = 0.0, total_sk_time = 0.0;
    int iterations = 100;

    if(argc > 1){
        iterations = atoi(argv[1]);
    }

    for(int j=0; j<iterations; j++){
    
        for (i=0; i<48; i++)
            entropy_input[i] = i;
        randombytes_init(entropy_input, NULL, 256);

        randombytes(seed, 48);

        randombytes_init(seed, NULL, 256);

        ct = malloc(crypto_kem_CIPHERTEXTBYTES);
        ss = malloc(crypto_kem_BYTES);
        ss1 = malloc(crypto_kem_BYTES);
        pk = malloc(crypto_kem_PUBLICKEYBYTES);
        sk = malloc(crypto_kem_SECRETKEYBYTES);

        start = clock();
        if ( (ret_val = crypto_kem_keypair(pk, sk)) != 0) {
            printf("crypto_kem_keypair returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;
        printf(" keypair took %lfs\n\n", time);


        start = clock();
        if ( (ret_val = crypto_kem_enc(ct, ss, pk)) != 0) {
            printf("crypto_kem_enc returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;
        printf(" encaps took %lfs\n\n", time);

        start = clock();
        if ( (ret_val = crypto_kem_dec(ss1, ct, sk)) != 0) {
            printf("crypto_kem_dec returned <%d>\n", ret_val);
            return KAT_CRYPTO_FAILURE;
        }
        end = clock();
        time = ((double) (end-start)/CLOCKS_PER_SEC);
        total_time += time;
        printf(" decaps took %lfs\n\n", time);


        if ( memcmp(ss, ss1, crypto_kem_BYTES) ) {
            printf("Test: FAIL\n");
            return KAT_CRYPTO_FAILURE;
        }else{
            printf("Test: PASS\n");
        }
        for(int i = 0 ; i < 32 ; ++i) printf("%x", ss1[i]);
        printf("\nTotal time %lfs\n", total_time);
        printf("\n\n----------------------------------------------\n");
        total_sk_time += sk_time;
        total_time_mean += total_time;
        total_time = 0.0;
        sk_time = 0.0;
    }

    printf("\n%lf\n", total_sk_time/iterations);
#endif

    return 0;
}
