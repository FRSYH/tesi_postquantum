/*
  This file is for public-key generation
*/

#ifndef PK_GEN_H
#define PK_GEN_H

#include <stdint.h>

extern double pk_time;
extern double pk_time_t;

int pk_gen(unsigned char *, const unsigned char *, uint32_t *);

#endif

