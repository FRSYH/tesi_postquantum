#!/usr/bin/env python3

import os
import sys
import time

def startProgress(title):
    global progress_x
    sys.stderr.write(title + ": [" + "-"*40 + "]" + chr(8)*41)
    sys.stderr.flush()
    progress_x = 0

def progress(x):
    global progress_x
    x = int(x * 40 // 100)
    sys.stderr.write("#" * (x - progress_x))
    sys.stderr.flush()
    progress_x = x

def endProgress():
    sys.stderr.write("#" * (40 - progress_x) + "]\n")
    sys.stderr.flush()

def percentage(it, max_it):
    return (it * 100) / max_it

myCmd = './main_mceliece 1'
iterations = 200


start_time = time.time()
os.system(myCmd)
finish_time = time.time()
duration = finish_time - start_time
sys.stderr.write(f'running {myCmd} for {iterations} iterations\n')
sys.stderr.write(f'estimated time to complete {duration*iterations} sec\n')
startProgress('main_maceliece')
for x in range(iterations):  
    progress(percentage(x+1, iterations))
    os.system(myCmd)
endProgress()
