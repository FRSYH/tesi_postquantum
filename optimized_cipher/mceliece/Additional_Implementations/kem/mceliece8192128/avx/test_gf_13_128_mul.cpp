#include "gf.h"
#include "params.h"
#include <gf2x/gf2x-small.h>
#include <gf2x.h>
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <ctime>
/*

 g++ -o test_gf_13_128_mul test_gf_13_128_mul.cpp gf.c -mpclmul -fopenmp -O3 -march=native -mtune=native

*/

using namespace std;


int main(void){


    int iterations = 128;

    srand(time(0));
    gf x[128] = {0};
    gf y[128] = {0};
    gf c[128] = {0};
    gf coeff;

    for(long i=0; i<iterations; i++){
        coeff = rand() % 8191;
        x[i] = coeff;
        coeff = rand() % 8191;
        y[i] = coeff;
    }

    for(int i = 0; i < iterations; i++){ 
        auto begin = std::chrono::high_resolution_clock::now();
        gf_mul_13_128(c, x, y);
        auto finish = std::chrono::high_resolution_clock::now();           
        double duration = chrono::duration_cast<chrono::nanoseconds>(finish-begin).count();
        cout << duration << "\n";
    }

    double start = omp_get_wtime();
    for(int i = 0; i < iterations; i++){
        gf_mul_13_128(c, x, y);
    }
    double end = omp_get_wtime();    

    auto begin = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < iterations; i++){ 
        gf_mul_13_128(c, x, y);
    }
    auto finish = std::chrono::high_resolution_clock::now();           
    double duration = chrono::duration_cast<chrono::nanoseconds>(finish-begin).count();

    printf("gf_mul over %d iterations\n", iterations);
    printf("total time %lf sec\n", end - start);
    cout << "total time " << duration << " ns\n";
    cout << "mean " << (duration/(iterations)) << " ns\n";

    return 0;
}