/*
   PQCgenKAT_kem.c
   Created by Bassham, Lawrence E (Fed) on 8/29/17.
   Copyright © 2017 Bassham, Lawrence E (Fed). All rights reserved.
   + mods from djb: see KATNOTES
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rng.h"
#include "crypto_kem.h"
#include <omp.h>

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_CRYPTO_FAILURE  -4
#define KAT_TEST 1


void	fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L);

unsigned char entropy_input[48];
unsigned char seed[KATNUM][48];

int
main()
{
    FILE                *fp_req, *fp_rsp;
    int                 ret_val;
    int i;
    unsigned char *ct = 0;
    unsigned char *ss = 0;
    unsigned char *ss1 = 0;
    unsigned char *pk = 0;
    unsigned char *sk = 0;
    double start, finish, time;
    double kat_times[KATNUM] = {0.0};
    int iterations = 2000;
    int k;

        for (i=0; i<48; i++)
            entropy_input[i] = i;
        randombytes_init(entropy_input, NULL, 256);

        for (i=0; i<KATNUM; i++)
            randombytes(seed[i], 48);

        fp_req = fdopen(8, "w");
        if (!fp_req)
            return KAT_FILE_OPEN_ERROR;

        for (i=0; i<KATNUM; i++) {
            fprintBstr(fp_req, "seed = ", seed[i], 48);
            fprintf(fp_req, "pk =\n");
            fprintf(fp_req, "sk =\n");
            fprintf(fp_req, "ct =\n");
            fprintf(fp_req, "ss =\n\n");
        }

        fp_rsp = fdopen(9, "w");
        if (!fp_rsp)
            return KAT_FILE_OPEN_ERROR;

        //fprintf(fp_rsp, "# kem/%s\n\n", crypto_kem_PRIMITIVE);

        for (i=0; i<KAT_TEST; i++) {
            
            for(k = 0; k < iterations; k++){

                if (!ct) ct = malloc(crypto_kem_CIPHERTEXTBYTES);
                if (!ct) abort();
                if (!ss) ss = malloc(crypto_kem_BYTES);
                if (!ss) abort();
                if (!ss1) ss1 = malloc(crypto_kem_BYTES);
                if (!ss1) abort();
                if (!pk) pk = malloc(crypto_kem_PUBLICKEYBYTES);
                if (!pk) abort();
                if (!sk) sk = malloc(crypto_kem_SECRETKEYBYTES);
                if (!sk) abort();

                randombytes_init(seed[i], NULL, 256);

                //fprintBstr(fp_rsp, "seed = ", seed[i], 48);
                
                start = omp_get_wtime();
                if ( (ret_val = crypto_kem_keypair(pk, sk)) != 0) {
                    fprintf(stderr, "crypto_kem_keypair returned <%d>\n", ret_val);
                    return KAT_CRYPTO_FAILURE;
                }
                finish = omp_get_wtime();
                time = finish - start;
                kat_times[i] += time;
                printf("keypair took %lf\n", time);

                fprintf(fp_rsp, "%lf", time);

                //fprintBstr(fp_rsp, "pk = ", pk, crypto_kem_PUBLICKEYBYTES);
                //fprintBstr(fp_rsp, "sk = ", sk, crypto_kem_SECRETKEYBYTES);
                
                if ( (ret_val = crypto_kem_enc(ct, ss, pk)) != 0) {
                    fprintf(stderr, "crypto_kem_enc returned <%d>\n", ret_val);
                    return KAT_CRYPTO_FAILURE;
                }
                //fprintBstr(fp_rsp, "ct = ", ct, crypto_kem_CIPHERTEXTBYTES);
                //fprintBstr(fp_rsp, "ss = ", ss, crypto_kem_BYTES);
                
                fprintf(fp_rsp, "\n");
                
                if ( (ret_val = crypto_kem_dec(ss1, ct, sk)) != 0) {
                    fprintf(stderr, "crypto_kem_dec returned <%d>\n", ret_val);
                    return KAT_CRYPTO_FAILURE;
                }
                
                if ( memcmp(ss, ss1, crypto_kem_BYTES) ) {
                    fprintf(stderr, "crypto_kem_dec returned bad 'ss' value\n");
                    return KAT_CRYPTO_FAILURE;
                }
            }
        }
    

    printf("\n\n------- STATISTICS --------\n");
    printf("Mean times over %d iteration of the NIST KAT\n", iterations);
    for(k=0; k<KAT_TEST; k++){
        printf("KAT %d = %lf\n", k, kat_times[k]/iterations);
    }
    printf("-------------------------\n");
    return KAT_SUCCESS;
}

void
fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L)
{
	unsigned long long i;

	fprintf(fp, "%s", S);

	for ( i=0; i<L; i++ )
		fprintf(fp, "%02X", A[i]);

	if ( L == 0 )
		fprintf(fp, "00");

	fprintf(fp, "\n");
}
