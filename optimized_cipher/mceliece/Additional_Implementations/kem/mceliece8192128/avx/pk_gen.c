/*
  This file is for public-key generation
*/
#define _GNU_SOURCE
#include "pk_gen.h"

#include "controlbits.h"
#include "params.h"
#include "benes.h"
#include "util.h"
#include "fft.h"
#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <omp.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "crypto_kem.h"
#include <stdlib.h>
#include <string.h>

#define HEIGHT GFBITS * SYS_T
#define GAUSS_WIDTH ((GFBITS * SYS_T) / 64)
#define WIDTH SYS_T
#define INVERTED_STOP 102

#define NUM_THREADS 4

double pk_time = 0.0;
double pk_time_t = 0.0;

static void de_bitslicing(uint64_t * out, const vec256 in[][GFBITS])
{
	int i, j, r;
	uint64_t u = 0;

	for (i = 0; i < (1 << GFBITS); i++)
		out[i] = 0 ;

	for (i = 0; i < 32; i++)
	{
		for (j = GFBITS-1; j >= 0; j--)
		{
			u = vec256_extract(in[i][j], 0); 
			for (r = 0; r < 64; r++) { out[i*256 + 0*64 + r] <<= 1; out[i*256 + 0*64 + r] |= (u >> r) & 1;}
			u = vec256_extract(in[i][j], 1);
			for (r = 0; r < 64; r++) { out[i*256 + 1*64 + r] <<= 1; out[i*256 + 1*64 + r] |= (u >> r) & 1;}
			u = vec256_extract(in[i][j], 2); 
			for (r = 0; r < 64; r++) { out[i*256 + 2*64 + r] <<= 1; out[i*256 + 2*64 + r] |= (u >> r) & 1;}
			u = vec256_extract(in[i][j], 3);
			for (r = 0; r < 64; r++) { out[i*256 + 3*64 + r] <<= 1; out[i*256 + 3*64 + r] |= (u >> r) & 1;}
		}
	}
}

static void to_bitslicing_2x(vec256 out0[][GFBITS], vec256 out1[][GFBITS], const uint64_t * in)
{
	int i, j, k, r;
	uint64_t u[4];

	for (i = 0; i < 32; i++)
	{
		for (j = GFBITS-1; j >= 0; j--)
		{
			for (k = 0; k < 4; k++)
			for (r = 63; r >= 0; r--)
			{
				u[k] <<= 1;
				u[k] |= (in[i*256 + k*64 + r] >> (j + GFBITS)) & 1;
			}
        
			out1[i][j] = vec256_set4x(u[0], u[1], u[2], u[3]);
		}

		for (j = GFBITS-1; j >= 0; j--)
		{
			for (k = 0; k < 4; k++)
			for (r = 63; r >= 0; r--)
			{
				u[k] <<= 1;
				u[k] |= (in[i*256 + k*64 + r] >> j) & 1;
			}
        
			out0[i][GFBITS-1-j] = vec256_set4x(u[0], u[1], u[2], u[3]);
		}
	}
}

int pk_gen(unsigned char * pk, const unsigned char * irr, uint32_t * perm)
{
	const int nBlocks_H = (SYS_N + 255) / 256;
	const int nBlocks_I = (GFBITS * SYS_T + 255) / 256;

	int i, j, k;
	int row, c, d, col;
	int flag = 0;

	uint64_t mat[ GFBITS * SYS_T ][ 102 ];
	uint64_t mat_t[28][1664];
	uint64_t ops_t[26][1664] = {0};

	uint64_t mask;	

	vec128 sk_int[ GFBITS ];

	vec256 consts[ 32 ][ GFBITS ];
	vec256 eval[ 32 ][ GFBITS ];
	vec256 prod[ 32 ][ GFBITS ];
	vec256 tmp[ GFBITS ];

	uint64_t list[1 << GFBITS];
	uint64_t one_row[ (SYS_N - GFBITS*SYS_T)/64 ];

	// compute the inverses
	double startTime = omp_get_wtime();
	irr_load(sk_int, irr);

	fft(eval, sk_int);

	vec256_copy(prod[0], eval[0]);

	for (i = 1; i < 32; i++)
		vec256_mul(prod[i], prod[i-1], eval[i]);

	vec256_inv(tmp, prod[31]);

	for (i = 30; i >= 0; i--)
	{
		vec256_mul(prod[i+1], prod[i], tmp);
		vec256_mul(tmp, tmp, eval[i+1]);
	}

	vec256_copy(prod[0], tmp);

	double stopTime = omp_get_wtime();
	double time = stopTime - startTime;	
	printf(" compute invers %lfs\n", time);


	// fill matrix 
	startTime = omp_get_wtime();
	de_bitslicing(list, prod);

	for (i = 0; i < (1 << GFBITS); i++)
	{	
		list[i] <<= GFBITS;
		list[i] |= i;	
		list[i] |= ((uint64_t) perm[i]) << 31;
	}

	sort_63b(1 << GFBITS, list);

	to_bitslicing_2x(consts, prod, list);

	for (i = 0; i < (1 << GFBITS); i++)
		perm[i] = list[i] & GFMASK;


	for (j = 0; j < nBlocks_I; j++)
	for (k = 0; k < GFBITS; k++)
	{
		mat_t[ 27 - (4*j + 0) ][ k ] = vec256_extract(prod[ j ][ k ], 0);
		mat_t[ 27 - (4*j + 1) ][ k ] = vec256_extract(prod[ j ][ k ], 1);
		mat_t[ 27 - (4*j + 2) ][ k ] = vec256_extract(prod[ j ][ k ], 2);
		mat_t[ 27 - (4*j + 3) ][ k ] = vec256_extract(prod[ j ][ k ], 3);
	}

	for (i = 1; i < SYS_T; i++)
	for (j = 0; j < nBlocks_I; j++)
	{
		vec256_mul(prod[j], prod[j], consts[j]);

		for (k = 0; k < GFBITS; k++)
		{
			mat_t[ 27 - (4*j + 0) ][ i*GFBITS + k ] = vec256_extract(prod[ j ][ k ], 0);
			mat_t[ 27 - (4*j + 1) ][ i*GFBITS + k ] = vec256_extract(prod[ j ][ k ], 1);
			mat_t[ 27 - (4*j + 2) ][ i*GFBITS + k ] = vec256_extract(prod[ j ][ k ], 2);
			mat_t[ 27 - (4*j + 3) ][ i*GFBITS + k ] = vec256_extract(prod[ j ][ k ], 3);
		}
	}
	
	
	// copia delle prime due righe di mat_t nelle prime due colonne di mat
	for(i = 0; i < HEIGHT; i++){
		mat[i][0] = mat_t[1][i];
		mat[i][1] = mat_t[0][i];
	}

	stopTime = omp_get_wtime();
	time = stopTime - startTime;	
	printf(" fill matrix %lfs\n", time);


	// gaussian elimination to obtain an upper triangular matrix 
	// and keep track of the operations in ops
	for (i = 0; i < (GFBITS * SYS_T) / 64; i++)
	for (j = 0; j < 64; j++)
	{
		col = i*64 + j;			

		ops_t[ 25 - i ][ col ] = 1;
		ops_t[ 25 - i ][ col ] <<= j;
	}
	
	double transpose_start = omp_get_wtime();
	/*
	uint64_t saved_mask[HEIGHT];  mat_t[0][]
	uint64_t saved_pivot[HEIGHT]; mat_t[1][]
	*/
	int num_threads = NUM_THREADS;
	int thread_window = (GAUSS_WIDTH / num_threads) + 1;
	int start, end, window_size;
	omp_set_num_threads(num_threads);
	#pragma omp parallel private(i, j, col, c, mask, k, start, end) shared(mat_t, ops_t, thread_window)
	{	
		int tid = omp_get_thread_num();
		start = 2 + (tid * thread_window);
		end = start + thread_window;
		if(end > 28){
			end = 28;
		}

		for(i = 27; i >= 2; i--){
			for(j = 0; j < 64; j++){
				col = ((27 - i) * 64) + j;

				#pragma omp single
				{
					for(k = col+1; k < HEIGHT; k++){
						mask = mat_t[i][col] >> j;
						mask &= 1;
						mask -= 1;
						mat_t[i][col] ^= mat_t[i][k] & mask;
						ops_t[(i-2)][col] ^= ops_t[(i-2)][k] & mask;
						mat_t[0][k-1] = mask;
					}
				}

				for(c = start; c < end; c++){
					if(c != i){
						for(k = col+1; k < HEIGHT; k++){
							mat_t[c][col] ^= mat_t[c][k] & mat_t[0][k-1];
							ops_t[(c-2)][col] ^= ops_t[(c-2)][k] & mat_t[0][k-1];
						}
					}
				}

				if( ((mat_t[i][col] >> j) & 1) == 0 ){
					flag = 1;
				}


				#pragma omp single
				{				
					for(k = col+1; k < HEIGHT; k++){
						mask = mat_t[i][k] >> j;
						mask &= 1;
						mask = -mask;			
						mat_t[1][k] = mask;
					}
				}

				for(c = start; c < end; c++){
					for(k = col+1; k < HEIGHT; k++){
						mat_t[c][k] ^= mat_t[c][col] & mat_t[1][k];
						ops_t[(c-2)][k] ^= ops_t[(c-2)][col] & mat_t[1][k];
					}
				}
				#pragma omp barrier
			}
		}
	}

	double transpose_stop = omp_get_wtime();
	time = transpose_stop - transpose_start;
	pk_time_t += time;	
	printf(" transpose gauss %lfs\n", time);

	if(flag == 1){
		printf(" pk not systematic\n");
		return -1;		
	}

	// computing the lineaer map required to obatin the systematic form
	startTime = omp_get_wtime();
	num_threads = NUM_THREADS;
	thread_window = (GAUSS_WIDTH / num_threads) + 1;
	omp_set_num_threads(num_threads);

	#pragma omp parallel private(i, j, col, k, c, mask) shared(ops_t, mat_t)
	{
		int tid = omp_get_thread_num();
		start = tid * thread_window;
		end = start + thread_window;
		if(end > GAUSS_WIDTH){
			end = GAUSS_WIDTH;
		}

		for (i = 0; i < 26; i++){
			for (j = 63; j >= 0; j--){
				col = ((GAUSS_WIDTH-1) - i)*64 + j;	
				for(c = start; c < end; c++){
					for (k = 0; k < col; k++){
						mask = mat_t[ 2 + i ][ k ] >> j;
						mask &= 1;
						mask = -mask;
						ops_t[ c ][ k ] ^= ops_t[ c ][ col ] & mask;
					}
				}
			}			
		}
	}

	stopTime = omp_get_wtime();
	time = stopTime - startTime;	
	printf(" compute linear map transpose %lfs\n", time);
	
	int z;

	startTime = omp_get_wtime();
	// apply the linear map to the non-systematic part
	for (j = nBlocks_I, z = 0; j < nBlocks_H && z < 25; j++, z++)
	for (k = 0; k < GFBITS; k++)
	{
		mat[ k ][ 4*z + 2 ] = vec256_extract(prod[ j ][ k ], 0);
		mat[ k ][ 4*z + 3 ] = vec256_extract(prod[ j ][ k ], 1);
		mat[ k ][ 4*z + 4 ] = vec256_extract(prod[ j ][ k ], 2);
		mat[ k ][ 4*z + 5 ] = vec256_extract(prod[ j ][ k ], 3);
	}

	for (i = 1; i < SYS_T; i++)
	for (j = nBlocks_I, z = 0; j < nBlocks_H && z < 25; j++, z++)
	{
		vec256_mul(prod[j], prod[j], consts[j]);

		for (k = 0; k < GFBITS; k++)
		{
			mat[ i*GFBITS + k ][ 4*z + 2 ] = vec256_extract(prod[ j ][ k ], 0);
			mat[ i*GFBITS + k ][ 4*z + 3 ] = vec256_extract(prod[ j ][ k ], 1);	
			mat[ i*GFBITS + k ][ 4*z + 4 ] = vec256_extract(prod[ j ][ k ], 2);
			mat[ i*GFBITS + k ][ 4*z + 5 ] = vec256_extract(prod[ j ][ k ], 3);	
		}
	}

	// traspongo ops_t in mat_t in modo tale che le colonne da 26 elementi di ops_t siano memorizzate sulle righe di mat_t
	// in teoria gli accessi in memoria dovrebbero essere efficiente senza sprecare memoria
	int r;
	i = 0;
	j = 0;
	for(c=0; c<1664; c++){
		for(r=25; r>=0; r--){
			mat_t[i][j] = ops_t[r][c];
			j++;
			if(j == 1664){
				j = 0;
				i++;		
			}
		}
	} 

	num_threads = NUM_THREADS;
	thread_window = (26/num_threads) + 1 ;
	int pk_window = 0;

	omp_set_num_threads(num_threads);
	#pragma omp parallel private(i, j, k, c, d, mask, start, end) shared(ops_t, mat, mat_t)
	{
		int tid = omp_get_thread_num();
		start = tid * thread_window;
		end = start + thread_window;
		pk_window = (end - start) * 64;
		if(end > 26){
			end = 26;
		}

		unsigned char * pkk;
		pkk = pk + (tid * pk_window * 816);

		for (i = start; i <end; i++){
			for (j = 0; j < 1664; j+= 26){
				for (k = 0; k < 102; k++)
					ops_t[tid][ k ] = 0;

				for (c = 0; c < 26; c++){
					for (d = 0; d < 64; d++){
						mask = mat_t[ i ][ j + c ] >> d;
						mask &= 1;
						mask = -mask;

						for (k = 0; k < 102; k++)
							ops_t[tid][ k ] ^= mat[ c*64 + d ][ k ] & mask;
					}
				}
				for (k = 0; k < 102; k++){
					store8(pkk, ops_t[tid][ k ]);
					pkk += 8;		
				}			
			}
		}
	}
	
	stopTime = omp_get_wtime();
	time = stopTime - startTime;	
	printf(" apply linera map %lfs\n", time);
	//


	return 0;
}

