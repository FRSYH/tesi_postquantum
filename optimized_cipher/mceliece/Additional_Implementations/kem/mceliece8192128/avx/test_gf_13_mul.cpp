#include "gf.h"
#include "params.h"
#include <gf2x/gf2x-small.h>
#include <gf2x.h>
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <inttypes.h>
/*

g++ -o test_gf_13_mul test_gf_13_mul.cpp -mpclmul -fopenmp -O3 -march=native -mtune=native

*/

using namespace std;


static inline uint32_t direct_mul1(unsigned long a, unsigned long b){
    __m128i aa = _gf2x_mm_setr_epi64(a, 0);
    __m128i bb = _gf2x_mm_setr_epi64(b, 0);
	return (uint32_t) _mm_cvtsi128_si64x(_mm_clmulepi64_si128(aa, bb, 0));
}

inline gf  gf_mul(gf in0, gf in1){
	
	uint64_t t, prod;
	prod = direct_mul1(in0, in1);

	//riduzione in modulo
	t = prod & 0x1FF0000;
	prod ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	t = prod & 0x000E000;
	prod ^= (t >> 9) ^ (t >> 10) ^ (t >> 12) ^ (t >> 13);

	return prod & GFMASK;
}


int main(void){



    gf a = 1500;
    gf b = 3565;
    uint64_t res = 0UL;
    uint64_t iterations = 10000000;

    double start = omp_get_wtime();
    double begin = omp_get_wtick();
    for(uint64_t i = 0; i < iterations; i++){
        res += gf_mul(a, b);
    }
    double finish = omp_get_wtick();
    double end = omp_get_wtime();    
    printf("res %" PRIu64 "\n", res);
    printf("gf_mul over %" PRIu64 " iterations\n", iterations);
    printf("total time %lf sec\n", end - start);
    printf("total cycles %lf \n", finish - begin);


    return 0;
}