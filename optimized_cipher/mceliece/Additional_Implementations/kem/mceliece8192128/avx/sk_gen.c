/*
  This file is for secret-key generation
*/

#include "sk_gen.h"
#include "randombytes.h"
#include "controlbits.h"
#include "params.h"
#include "util.h"
#include "gf.h"
#include <omp.h>
#include <time.h>
#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>
#include <m4rie/m4rie.h>


#define PARALLEL

/* input: f, element in GF((2^m)^t) */
/* output: out, minimal polynomial of f */
/* return: 0 for success and -1 for failure */
int genpoly_gen(gf *out, gf *f)
{
	int i, j, k, c;

	gf mat[ SYS_T+1 ][ SYS_T ];
	gf mask, inv;

	// fill matrix

	mat[0][0] = 1;

	for (i = 1; i < SYS_T; i++)
		mat[0][i] = 0;

	for (i = 0; i < SYS_T; i++)
		mat[1][i] = f[i];


	double elapsed;
	double startTime = omp_get_wtime();	

	for (j = 2; j <= SYS_T; j++)
		gf_mul_13_128(mat[j], mat[j-1], f);
	
	double stopTime = omp_get_wtime();
	elapsed = stopTime - startTime;
	printf("\n gf_mul_13_128 %lfs\n", elapsed);

    gf2e *ff;
    int field = GFBITS;
    rci_t m, n, r;
    m = SYS_T;
    n = SYS_T + 1;
    ff = gf2e_init(irreducible_polynomials[field][1]);
	mzed_t *A  = mzed_init(ff, m, n);

	for(i = 0; i < SYS_T + 1; i++){
		for(j = 0; j < SYS_T; j++){
			mzed_write_elem(A, j, i, mat[i][j]);
		}
	}

	// gaussian
	int flag = 0;

	startTime = omp_get_wtime();
    rci_t res = mzed_echelonize_naive(A, 1); 
	stopTime = omp_get_wtime();
	elapsed = stopTime - startTime;
	printf(" gauss M4RIE %lfs\n", elapsed);

	if(flag == 1){
		printf("SK NOT SYSTEMATIC\n");
		return -1;
	}

	for (i = 0; i < SYS_T; i++)
		out[i] = mzed_read_elem(A, i, SYS_T);

	return 0;
}

/* input: permutation p represented as a list of 32-bit intergers */
/* output: -1 if some interger repeats in p */
/*          0 otherwise */
int perm_check(uint32_t *p)
{
	int i;
	uint64_t list[1 << GFBITS];

	for (i = 0; i < (1 << GFBITS); i++)
		list[i] = p[i];
        
	sort_63b(1 << GFBITS, list);
        
	for (i = 1; i < (1 << GFBITS); i++)
		if (list[i-1] == list[i])
			return -1;

	return 0;
}

